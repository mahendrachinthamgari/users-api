const loader = document.querySelector('.lds-spinner');
const loaderContainer = document.querySelector('.loader-container');

fetch('https://fakestoreapi.com/users')
    .then((response) => {
        return response.json();
    }).then((data) => {
        if (data) {
            console.log("Fetch sucessfull");
            usersData(data);
            loader.classList.remove('lds-spinner');
            loaderContainer.classList.remove('loader-container');
        } else {
            loader.classList.remove('lds-spinner');
            loaderContainer.classList.remove('loader-container');
            fetchSucessZeroData();
        }
    }).catch((err) => {
        console.error("Error occured");
        console.error(err);
        loader.classList.remove('lds-spinner');
        loaderContainer.classList.remove('loader-container');
        errorFunctionCall();
    });


function usersData(data) {

    const ul = document.querySelector('ul');
    const header = document.querySelector('header');

    header.style.display = 'block';

    if (Array.isArray(data)) {
        data.forEach((user) => {
            let li = document.createElement('li');
            let id = document.createElement('p');
            let fullName = document.createElement('p');
            let userName = document.createElement('p');
            let phone = document.createElement('p');
            let address = document.createElement('p');


            id.textContent = "id: " + user.id;
            fullName.textContent = "Fullname: " + (user.name.firstname + ' ' + user.name.lastname).toUpperCase();
            userName.textContent = "Username: " + user.username;
            phone.textContent = "Phone: " + user.phone;
            address.textContent = "Address: " + user.address.number + ', ' + user.address.city + ', ' + user.address.street + ' -' + user.address.zipcode;


            li.append(id, fullName, userName, phone, address);
            ul.append(li);

        });
    } else {
        let dataArray = [];
        dataArray.push(data);

        usersData(dataArray)
    }
}

function errorFunctionCall() {

    const ul = document.querySelector('ul');

    let divError = document.createElement('div');
    let h2 = document.createElement('h2');

    divError.setAttribute('class', 'error');

    h2.textContent = 'Fetch faild, please try again!';

    divError.append(h2);
    ul.append(divError);

}

function fetchSucessZeroData() {
    const ul = document.querySelector('ul');

    let divZerodata = document.createElement('div');
    let h2 = document.createElement('h2');

    divZerodata.setAttribute('class', 'zero-data');

    h2.textContent = 'Nothing to show!';

    divZerodata.append(h2);
    ul.append(divZerodata);

}